import requests
import platform

from typing import (
    TYPE_CHECKING,
    Dict,
    List,
    Literal,
    Optional,
    Type,
    overload,
    Any,
    TypeVar,
    Union,
    TypedDict,
)

from enum import IntEnum

__all__ = ["Torrent", "Category", "API"]
T = TypeVar(name="T", bound="Torrent")
C = TypeVar(name="C", bound="Category")


class EpisodeInfoData(TypedDict):
    imdb: Optional[str]
    tvrage: Optional[str]
    tvdb: Optional[str]
    themoviedb: Optional[str]


class Torrent:
    """Representação de um torrent,

    Attributes
    ----------
    download: :class:`str`
        magnet url para baixar o torrent
    category: :class:`Category`
        Categoria do torrent
    title: `Optional[str]`
        Tituto do torrent
    filename: `Optional[str]`
        Filename do torrent,
    seeders: `Optional[int]`
        Quantidade de seeders o torrent têm
    leechers: `Optional[int]`
        Quantidade de leechers o torrent têm
    size: `Optional[int]`
        Tamanho do torrent em bytes
    pubdate: `Optional[str]`
        Data de quando esse torrent foi publicado na api
    episode_info: `EpisodeInfoData`
        Imformação sobre o episodio / filme
    ranked: `Optional[int]`
        Posição de ranking onde o filme está
    info_page: `Optional[str]`
        Link da pagina do filme na api
    """

    def __init__(
        self,
        download: str,
        category: Union[C, str],
        title: Optional[str] = None,
        filename: Optional[str] = None,
        seeders: Optional[int] = 0,
        leechers: Optional[int] = 0,
        size: Optional[int] = 0,
        pubdate: Optional[str] = None,
        episode_info: Optional[EpisodeInfoData] = None,
        ranked: Optional[int] = 0,
        info_page: Optional[str] = None,
    ) -> None:
        self.title: Optional[str] = title
        self.filename: Optional[str] = filename
        self.category: Union[C, str] = category
        self.download: str = download
        self.seeders: Optional[int] = seeders
        self.leechers: Optional[int] = leechers
        self.size: Optional[int] = size
        self.pubdate: Optional[str] = pubdate
        self.episode_info: Optional[EpisodeInfoData] = episode_info
        self.ranked: Optional[int] = ranked
        self.info_page: Optional[str] = info_page

    def __repr__(self) -> str:
        return (
            f"<{self.__class__.__name__} title={self.title} filename={self.filename} "
            f"seeders={self.seeders} leechers={self.leechers} size={self.size}>"
        )

    @classmethod
    def from_json(cls: Type[T], data: Dict[Any, Any]) -> T:
        self = cls(**data)
        self._raw = data
        return self


class Category(IntEnum):
    adult = 4
    movie_xvid = 14
    movie_xvid_720p = 48
    movie_h264 = 17
    movie_h264_1080p = 44
    movie_h264_720p = 45
    movie_h264_3d = 47
    movie_h264_4k = 50
    movie_h265_4k = 51
    movie_h265_4k_hdr = 52
    movie_full_bd = 42
    movie_bd_remux = 46
    tv_episodes = 18
    tv_episodes_hd = 41
    tv_episodes_uhd = 49
    music_mp3 = 23
    music_flac = 25
    games_pc_iso = 27
    games_pc_rip = 28
    games_ps3 = 40
    games_ps4 = 53
    games_xbox = 32
    software = 33
    ebook = 35

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__} name={self.name}>"


class API:
    __slots__ = ("session", "_user_agent", "_token")
    BASE: str = "http://torrentapi.org/pubapi_v2.php"
    APP_ID: str = "rarbgapi"

    if TYPE_CHECKING:
        session: str
        _user_agent: str
        _token: str

    def __init__(self) -> None:
        self.session: requests.Session = self.__session()
        self._user_agent = "{appid}/{version} ({uname}) python {pyver}".format(
            appid=self.APP_ID,
            version=1.0,
            uname="; ".join(platform.uname()),
            pyver=platform.python_version(),
        )

        self._token = self._get_token()

    def __session(self) -> requests.Session:
        return requests.Session()

    def request(self, method, uri="", **params) -> Optional[requests.Response]:
        headers = {"user-agent": self._user_agent}

        params["app_id"] = self.APP_ID

        resp = self.session.request(
            method=method, url=self.BASE + uri, params=params, headers=headers
        )

        # rate limit
        if resp.status_code == 429:
            data = resp.text, resp.status_code, resp.reason

        resp.raise_for_status()
        return resp

    def _get_token(self) -> str:
        params = {"get_token": "get_token"}

        return self.request("GET", **params).json().get("token")

    @overload
    def query(
        self,
        mode: Literal["search"],
        *,
        search_string: str,
        extended_response: Optional[bool] = False,
        categories: Optional[List[Category]] = None,
        sort: Literal["last", "seeders", "leechers"] = None,
        limit: Optional[int] = None,
        min_seeders: Optional[int] = None,
        min_leechers: Optional[int] = None,
    ) -> Union[List[Torrent], Torrent]:
        ...

    @overload
    def query(
        self,
        mode: Literal["search"],
        *,
        search_imdb: str,
        extended_response: Optional[bool] = False,
        categories: Optional[List[Category]] = None,
        sort: Literal["last", "seeders", "leechers"] = None,
        limit: Optional[int] = None,
        min_seeders: Optional[int] = None,
        min_leechers: Optional[int] = None,
    ) -> Union[List[Torrent], Torrent]:
        ...

    @overload
    def query(
        self,
        mode: Literal["search"],
        *,
        search_tvdb: str,
        extended_response: Optional[bool] = False,
        categories: Optional[List[Category]] = None,
        sort: Literal["last", "seeders", "leechers"] = None,
        limit: Optional[int] = None,
        min_seeders: Optional[int] = None,
        min_leechers: Optional[int] = None,
    ) -> Union[List[Torrent], Torrent]:
        ...

    @overload
    def query(
        self,
        mode: Literal["search"],
        *,
        search_themoviedb: str,
        extended_response: Optional[bool] = False,
        categories: Optional[List[Category]] = None,
        sort: Literal["last", "seeders", "leechers"] = None,
        limit: Optional[int] = None,
        min_seeders: Optional[int] = None,
        min_leechers: Optional[int] = None,
    ) -> Union[List[Torrent], Torrent]:
        ...

    def query(
        self,
        mode: Literal["search", "list"],
        *,
        extended_response: Optional[bool] = True,
        categories: Optional[List[Category]] = None,
        sort: Literal["last", "seeders", "leechers"] = None,
        limit: Optional[int] = None,
        min_seeders: Optional[int] = None,
        min_leechers: Optional[int] = None,
        ranked: Optional[bool] = None,
        **op: Dict[Any, Any],
    ) -> Union[List[Torrent], Torrent]:
        """Fazer uma pesquisa para a api rarbg de acordo com os parametros recebidos

        .. note
            Apenas pode ser passado 1 tipo de pesquisa dos disponiveis.

        Paramtros
        ---------
        mode: `Literal["search", "list"]`
            Modo de pesquisa
        extended_response: `Optional[bool]`
            Formato de resposta que a api ira retornar,
            `json` ou `json_extended`
        sort: `Literal["last", "seeders", "leechers"]`
            Filtar a pesquisa de acordo com last, seeders ou leechers
        limit: `Optional[int]`
            Limitar a quandidade de resultados retornados
            pela api
        min_seeders: `Optional[int]`
            Filtrar pesquisa por apenas torrents
            com um minimo de :param:`min_seeders` seeders
        min_leechers: `Optional[int]`
            Filtrar pesquisa por apenas torrents
            com um minimo de :param:`min_leechers` leechers
        ranked: `Optional[bool]`
            Incluir/excluir torrents não ordenanos
        search_string: `Optional[str]`
            Pesquise torrent pelo nome
        search_imdb: `Optional[str]`
            Pesquise um torrent por um id imdb
        search_tvdb: `Optional[str]`
            Pesquise um torrent por um id tvdb
        search_themoviedb: `Optional[str]`
            Pesquise um torrent por um id themoviedb

        Retorna
        -------
        `List[Torrent]`
            Retorna todos os resoltados encontrados.

        Raises
        ------
        :class:`TypeError`
            Quando não é passado nenhum tipo de pesquisa
            dos disponivels, no modo de `search`
        """
        params = {"mode": mode, "token": self._token, "format": "json"}

        if extended_response:
            params["format"] = "json_extended"

        if categories:
            params["category"] = ";".join(str(x.value) for x in categories)

        if mode == "search":
            if search_string := op.pop("search_string"):
                params["search_string"] = search_string
            elif search_imdb := op.pop("search_imdb"):
                params["search_imdb"] = search_imdb
            elif search_tvdb := op.pop("search_tvdb"):
                params["search_tvdb"] = search_tvdb
            elif search_themoviedb := op.pop("search_themoviedb"):
                params["search_themoviedb"] = search_themoviedb
            else:
                raise TypeError(
                    "No search type param is passed (among search_string, search_imdb, search_tvdb, search_themoviedb)."
                )

        if sort:
            params["sort"] = sort

        if limit:
            params["limit"] = limit

        if min_seeders:
            params["min_seeders"] = min_seeders

        if min_leechers:
            params["min_leechers"] = min_leechers

        if ranked:
            params["ranked"] = ranked

        return [
            Torrent.from_json(x)
            for x in self.request("GET", **params).json().get("torrent_results", [])
        ]

    def list(
        self,
        categories: Optional[List[Category]] = None,
        extended_response: Optional[bool] = True,
        limit: Optional[int] = 25,
        min_seeders: Optional[int] = None,
        min_leechers: Optional[int] = None,
        ranked: Optional[bool] = False,
        sort: Literal["last", "seeders", "leechers"] = "last",
    ) -> Union[List[Torrent], Torrent]:
        return self.query(
            mode="list",
            categories=categories,
            extended_response=extended_response,
            limit=limit,
            min_seeders=min_seeders,
            min_leechers=min_leechers,
            sort=sort,
            ranked=ranked,
        )

    @overload
    def search(
        self,
        search_string: str,
        categories: Optional[List[Category]] = None,
        extended_response: Optional[bool] = True,
        limit: Optional[int] = 25,
        min_seeders: Optional[int] = None,
        min_leechers: Optional[int] = None,
        sort: Literal["last", "seeders", "leechers"] = "last",
    ) -> Union[List[Torrent], Torrent]:
        ...

    @overload
    def search(
        self,
        search_imdb: str,
        categories: Optional[List[Category]] = None,
        extended_response: Optional[bool] = True,
        limit: Optional[int] = 25,
        min_seeders: Optional[int] = None,
        min_leechers: Optional[int] = None,
        sort: Literal["last", "seeders", "leechers"] = "last",
    ) -> Union[List[Torrent], Torrent]:
        ...

    @overload
    def search(
        self,
        search_tvdb: str,
        categories: Optional[List[Category]] = None,
        extended_response: Optional[bool] = True,
        limit: Optional[int] = 25,
        min_seeders: Optional[int] = None,
        min_leechers: Optional[int] = None,
        sort: Literal["last", "seeders", "leechers"] = "last",
    ) -> Union[List[Torrent], Torrent]:
        ...

    @overload
    def search(
        self,
        search_themoviedb: str,
        categories: Optional[List[Category]] = None,
        extended_response: Optional[bool] = True,
        limit: Optional[int] = 25,
        min_seeders: Optional[int] = None,
        min_leechers: Optional[int] = None,
        sort: Literal["last", "seeders", "leechers"] = "last",
    ) -> Union[List[Torrent], Torrent]:
        ...

    def search(
        self,
        categories: Optional[List[Category]] = None,
        extended_response: Optional[bool] = True,
        limit: Optional[int] = 25,
        min_seeders: Optional[int] = None,
        min_leechers: Optional[int] = None,
        sort: Literal["last", "seeders", "leechers"] = "last",
        **op: Dict[Any, Any],
    ) -> Union[List[Torrent], Torrent]:
        return self.query(
            mode="search",
            categories=categories,
            extended_response=extended_response,
            limit=limit,
            min_seeders=min_seeders,
            min_leechers=min_leechers,
            sort=sort,
            **op,
        )


# testes

if __name__ == "__main__":
    c = API()

    c.list()
    search = c.search(search_string="dont look up")
    print(search)
