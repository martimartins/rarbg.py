# rarbg.py

Uma synchronous API wrapper da rarbg, que permite a pesquisa de filmes/series pirateados.

## Exemplo
```py
from rarbg import API, Torrent

client = API()
search = c.search(search_string="dont look up")
print(f"{search!r}")
```